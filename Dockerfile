FROM tensorflow/tensorflow:1.15.0-gpu-py3

RUN apt-get install -y \
build-essential \
curl \
git \
wget \
swig

RUN wget -c -O tensorflow-1.14.0-cp36-cp36m-linux_x86_64.whl "!!! PATH_FOR_TENSORWLOW_ABOVE_BULDED WHEEL !!!"
# remove preinstalled tensorflow with AVX support
RUN pip uninstall --yes tensorflow-gpu
# install custom build tensorwlow without AVX / AVX2 support
RUN pip install tensorflow-1.14.0-cp36-cp36m-linux_x86_64.whl

RUN pip --no-cache-dir install \
Pillow \
h5py \
keras_applications \
keras_preprocessing \
matplotlib \
mock \
numpy \
scipy \
sklearn \
pandas \
future \
portpicker \
enum34

RUN pip install jupyter matplotlib
RUN pip install jupyter_http_over_ws
RUN jupyter serverextension enable --py jupyter_http_over_ws

RUN mkdir -p /tf/tensorflow-tutorials && chmod -R a+rwx /tf/
RUN mkdir /.local && chmod a+rwx /.local
RUN apt-get install -y --no-install-recommends wget
WORKDIR /tf/tensorflow-tutorials
RUN wget https://raw.githubusercontent.com/tensorflow/docs/master/site/en/tutorials/keras/classification.ipynb
RUN wget https://raw.githubusercontent.com/tensorflow/docs/master/site/en/tutorials/keras/overfit_and_underfit.ipynb
RUN wget https://raw.githubusercontent.com/tensorflow/docs/master/site/en/tutorials/keras/regression.ipynb
RUN wget https://raw.githubusercontent.com/tensorflow/docs/master/site/en/tutorials/keras/save_and_load.ipynb
RUN wget https://raw.githubusercontent.com/tensorflow/docs/master/site/en/tutorials/keras/text_classification.ipynb RUN wget https://raw.githubusercontent.com/tensorflow/docs/master/site/en/tutorials/keras/text_classification_with_hub.ipynb RUN apt-get autoremove -y && apt-get remove -y wget WORKDIR /tf EXPOSE 8888

RUN python3 -m ipykernel.kernelspec CMD ["bash", "-c", "source /etc/bash.bashrc && jupyter notebook --notebook-dir=/tf --ip 0.0.0.0 --no-browser --allow-root"]


